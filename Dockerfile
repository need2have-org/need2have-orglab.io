FROM node:alpine

RUN apk add --no-cache build-base

WORKDIR /usr/app
COPY package.json .
RUN npm install --quiet
COPY . .
