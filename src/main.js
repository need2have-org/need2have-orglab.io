import Vue from 'vue'
import App from './App.vue'
import VueMqtt from 'vue-mqtt';
import Vuex from 'vuex'
import UUID from 'vue-uuid';
import 'es6-promise/auto'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import VueTimeago from 'vue-timeago'
import VueLodash from 'vue-lodash'
import lodash from 'lodash'

Vue.use(VueLodash, { lodash: lodash })

Vue.use(VueTimeago, {
  name: 'Timeago', // Component name, `Timeago` by default
  locale: 'de', // Default locale
  // We use `date-fns` under the hood
  // So you can use all locales from it
  locales: {
  }
})

Vue.use(UUID);

Vue.use(Vuex)
// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)

Vue.use(VueMqtt, 'wss://mqtt.need2have.org/mqtt', {
  clientId: "webservice_" + Math.random().toString(16).substr(2, 8),
  username: "webservice",
  password: "X17BuyLe??",
  clean: true,
  keepalive: 60
});

Vue.config.productionTip = false
Vue.prototype.$namespace = "corona"

new Vue({
  render: h => h(App),
}).$mount('#app')