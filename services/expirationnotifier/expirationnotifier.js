const readline = require('readline');
const _ = require('lodash');
const nodemailer = require("nodemailer");
const mailconfig = require('./mailconfig');

async function main() {
  // Generate test SMTP service account from ethereal.email
  // Only needed if you don't have a real mail account for testing
  let testAccount = await nodemailer.createTestAccount();

  // create reusable transporter object using the default SMTP transport
  let transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      host: config.server.host,
      port: config.server.port
    },
    pool: true
  });

  // send mail with defined transport object
  let info = await transporter.sendMail({
    from: '"Fred Foo 👻" <foo@example.com>', // sender address
    to: "bar@example.com, baz@example.com", // list of receivers
    subject: "Hello ✔", // Subject line
    text: "Hello world?", // plain text body
    html: "<b>Hello world?</b>" // html body
  });

  console.log("Message sent: %s", info.messageId);
  // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

  // Preview only available when sending through an Ethereal account
  console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
  // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
}

main().catch(console.error);
  

var needs = []
var haves = []
var needsNewlyExpired = []
var havesNewlyExpired = []
var needsPreviouslyExpired = []
var havessPreviouslyExpired = []

//let havesMap = new Map();
//let needsMap = new Map();

var EXPIRATION_PERIOD_MS = 24 * 60 * 60 * 1000 * 2;

// Not needed here. Warning messages shall be sent in another microservice.
var WARNING_PERIOD_MS = 24 * 60 * 60 * 1000 * 1;

// Assings ENV variables to an object
obj = process.env;

message_template = {
    topic: '',
    service_uuid: obj.SERVICE_UUID,
    service_name: obj.SERVICE_NAME,
    service_host: obj.SERVICE_HOST,
    created_at: ''
}

error_template = {
  topic: process.env.NAMESPACE_PUBLISHER + '/log',
  message: ''
}

// Reads JSON from stdin
var rl_stdin = readline.createInterface({
    input: process.stdin
});

//***********************
// NEW vs. UPDATED posts
// Updates are sent exactly the same way as new posts. Each post is reqired to
// have a uuid. We identify the old post by this uuid and update its updated_at
// and updated_ms values and reorder Map and Array. They need to be in order, 
// so iterating through them to find expired ones ist faster.
//***********************

// When line comes from stdin
rl_stdin.on('line', function(line){
    try {
        if (line !== null) {
            // First we check if the payload is valid JSON
            // FIXME: Schema validation missing - see webupdater service.
            parsed_json = JSON.parse(line);
            if (parsed_json.topic == obj.NAMESPACE_LISTENER + '/needs') {
              d = parsed_json.created_at;
              needs = parsed_json.payload; 
              var i;
              for (i = 0; i < needs.length; i++) {
                need = needs[i]
                if (need.updated_at) {
                  if (need.updated_at =< d - WARNING_PERIOD_MS) { needsNewlyExpired.push(need) }
                  if (need.updated_at > d - WARNING_PERIOD_MS) { break; }
                } elseif (need.created_at) {
                  if (need.created_at < d - WARNING_PERIOD_MS) { needsNewlyExpired.push(need) }                  
                  if (need.created_at > d - WARNING_PERIOD_MS) { break; }
                } else {
                  //#FIXME: need to add the actual JSON to the error message
                  console.log(JSON.stringify(Object.assign({}, error_template, {message: 'updated_at and created_at missing in need'})));
                }
              }
              needsNotificationQueue.push(_.difference(needsNewlyExpired, needsPreviouslyExpired));
            }
            if (parsed_json.topic == obj.NAMESPACE_LISTENER + '/haves') {
              d = parsed_json.created_at;
              haves = parsed_json.payload; 
              var i;
              for (i = 0; i < haves.length; i++) {
                have = haves[i]
                if (have.updated_at) {
                  if (have.updated_at =< d - WARNING_PERIOD_MS) { havesNewlyExpired.push(have) }
                  if (have.updated_at > d - WARNING_PERIOD_MS) { break; }
                } elseif (have.created_at) {
                  if (have.created_at < d - WARNING_PERIOD_MS) { havesNewlyExpired.push(have) }                  
                  if (have.created_at > d - WARNING_PERIOD_MS) { break; }
                } else {
                  //#FIXME: have to add the actual JSON to the error message
                  console.log(JSON.stringify(Object.assign({}, error_template, {message: 'updated_at and created_at missing in have'})));
                }
              }
              havesNotificationQueue.push(_.difference(havesNewlyExpired, havesPreviouslyExpired));
            }
            if (parsed_json.topic == obj.NAMESPACE_LISTENER + '/tick') {
              while (needs.length > 0 ) {
                  needToNotify = needs.shift();
                  
              }
              //SAMM MQTT adapter can yet not be told to set the retain option.
              //if (needs.length != needs_length_before) {
                publish_payload("needs", needs);
              //}
              //haves_length_before = haves.length;              
              while (haves.length > 0 && (new Date) - haves[0].updated_ms > EXPIRATION_PERIOD_MS ) {
                  havesMap.delete(haves[0].uuid);
                  haves.shift();
              }
              //if (haves.length != haves_length_before) {
                publish_payload("haves", haves);      
              //}        
            }
          }
    } catch (e) {
        console.log(JSON.stringify(Object.assign({}, error_template, {message: e.stack.toString()})));
    }
});

function publish_payload (topic, payload) {
  message_template.created_at = new Date().toISOString();              
  result = Object.assign({}, message_template, {payload: payload});
  result.topic = obj.NAMESPACE_PUBLISHER + '/' + topic;
  console.log(JSON.stringify(result));
  
}