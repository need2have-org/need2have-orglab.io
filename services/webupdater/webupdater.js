// FIXME: Either conveniently prefix the topic with "/" for the user, or not. 
// Currently, I prefix for publishing functions but not for subscriptions.
// Leads to misunderstandings.

var readline = require('readline');

var needs = []
var haves = []
var ONE_DAY = 24 * 60 * 60 * 1000;
const uuidv4regex = '^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-4[A-Fa-f\d]{3}-[89ABab][A-Fa-f\d]{3}-[A-Fa-f\d]{12}$';

// Assings ENV variables to an object
obj = process.env;

message_template = {
    topic: '',
    service_uuid: obj.SERVICE_UUID,
    service_name: obj.SERVICE_NAME,
    service_host: obj.SERVICE_HOST,
    created_at: ''
}

error_template = {
  topic: process.env.NAMESPACE_PUBLISHER + '/log',
  message: ''
}

post_schema = {
  "required": ["type", "email", "tel", "name", "address", "city", "country", "item", "amount", "uuid"],
  "properties": {
    "type": {
      "type": "string",
      "pattern": "need|have",
    },
    "email": {
      "type": "string",
      "format": "email",
    },
    "tel": {
      "type": "string",
    },
    "name": {
      "type": "string",
    },
    "address": {
      "type": "string",
    },
    "zip": {
      "type": "string",
    },
    "city": {
      "type": "string",
    },
    "country": {
      "type": "string",
    },
    "item": {
      "type": "string",
    },
    "amount": {
      "type": "number",
    },
    "uuid": {
      "type": "string",
      "format": "uuid"
    }
  }
}

const Ajv = require('ajv');
var ajv = new Ajv(); 
var validate = ajv.compile(post_schema);

const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const port = 80

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))
 
app.use(function (req, res) {
  if (req.query.update) {
    payload = JSON.parse(req.query.update)
    var valid = validate(payload);
    if (valid) {
      publish_payload(payload.type, payload);
      res.setHeader('Content-Type', 'text/plain')
      res.write('Your ' + payload.type + ' has been updated:\n')
      res.end(JSON.stringify(payload, null, 2))
    } else {
      res.setHeader('Content-Type', 'text/plain')
      res.write('Your payload is invalid:\n' + JSON.stringify(validate.errors) + '\n' )
      res.end(JSON.stringify(req.query, null, 2))    
    }
  } else {
    res.setHeader('Content-Type', 'text/plain')
    res.write('Query string needs to contain an "update" variable with a valid need/have JSON value:\n')
    res.end(JSON.stringify(req.query, null, 2))
  }
})

app.listen(port, () => publish_payload("log", {log_message: `Webupdater listening on port ${port}!`}))


// Reads JSON from stdin
var rl_stdin = readline.createInterface({
    input: process.stdin
});
// When line comes from stdin
rl_stdin.on('line', function(line){
    try {
        if (line !== null) {
            // first we check if the payload is valid JSON
            parsed_json = JSON.parse(line);
            if (parsed_json.topic == obj.NAMESPACE_LISTENER + '/tick') {
              publish_payload("log/alive", {type: "alive"});              
            }
          }
    } catch (e) {
        console.log(JSON.stringify(Object.assign({}, error_template, {message: e.stack.toString()})));
    }
});

function publish_payload (topic, payload) {
  message_template.created_at = new Date().toISOString();              
  result = Object.assign({}, message_template, {payload: payload});
  result.topic = obj.NAMESPACE_PUBLISHER + '/' + topic;
  console.log(JSON.stringify(result));
  
}