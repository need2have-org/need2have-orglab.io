var readline = require('readline');
const nodemailer = require("nodemailer");const nodemailer = require("nodemailer");

let transporter = nodemailer.createTransport(options[, defaults])
let poolConfig = "smtps://username:password@smtp.example.com/?pool=true";

var needs = []
var haves = []

let havesMap = new Map();
let needsMap = new Map();

var EXPIRATION_PERIOD_MS = 24 * 60 * 60 * 1000 * 2;

// Not needed here. Warning messages shall be sent in another microservice.
var WARNING_PERIOD_MS = 24 * 60 * 60 * 1000 * 1;

// Assings ENV variables to an object
obj = process.env;

message_template = {
    topic: '',
    service_uuid: obj.SERVICE_UUID,
    service_name: obj.SERVICE_NAME,
    service_host: obj.SERVICE_HOST,
    created_at: ''
}

error_template = {
  topic: process.env.NAMESPACE_PUBLISHER + '/log',
  message: ''
}

// Reads JSON from stdin
var rl_stdin = readline.createInterface({
    input: process.stdin
});

//***********************
// NEW vs. UPDATED posts
// Updates are sent exactly the same way as new posts. Each post is reqired to
// have a uuid. We identify the old post by this uuid and update its updated_at
// and updated_ms values and reorder Map and Array. They need to be in order, 
// so iterating through them to find expired ones ist faster.
//***********************

// When line comes from stdin
rl_stdin.on('line', function(line){
    try {
        if (line !== null) {
            // First we check if the payload is valid JSON
            // FIXME: Schema validation missing - see webupdater service.
            parsed_json = JSON.parse(line);
            if (parsed_json.topic == obj.NAMESPACE_LISTENER + '/need') {
              d = new Date();
              payload = Object.assign({}, parsed_json.payload);
              key = payload.uuid;
              val = needsMap.get(key);
              payload.updated_at = d.toISOString();
              payload.updated_ms = d.valueOf();
              if (val) {
                // In order to have the map sorted by updated_at, we delete and 
                // reinsert. This makes sending out warnings and purging expired
                // posts faster. Then we regenerate the needs Array.
                needsMap.delete(key);
                needsMap.set(payload.uuid, Object.assign(val, payload));
                needs = Array.from(needsMap.values());              
              } else {
                payload.created_at = d.toISOString();
                payload.created_ms = d.valueOf();
                // New posts can simply be added to the Map and the Array.
                needsMap.set(payload.uuid, payload);
                needs.push(parsed_json.payload)
              }
              publish_payload("needs", needs);
            }
            if (parsed_json.topic == obj.NAMESPACE_LISTENER + '/have') {
              d = new Date();
              payload = Object.assign({}, parsed_json.payload);
              key = payload.uuid;
              val = havesMap.get(key);
              payload.updated_at = d.toISOString();
              payload.updated_ms = d.valueOf();
              if (val) {
                // In order to have the map sorted by updated_at, we delete and 
                // reinsert. This makes sending out warnings and purging expired
                // posts faster. Then we regenerate the haves Array.
                havesMap.delete(key);
                havesMap.set(payload.uuid, Object.assign(val, payload));
                haves = Array.from(havesMap.values());              
              } else {
                payload.created_at = d.toISOString();
                payload.created_ms = d.valueOf();
                // New posts can simply be added to the Map and the Array.
                havesMap.set(payload.uuid, payload);
                haves.push(parsed_json.payload)
              }
              publish_payload("haves", haves);
            }
            if (parsed_json.topic == obj.NAMESPACE_LISTENER + '/tick') {
              //needs_length_before = needs.length;
              while (needs.length > 0 && (new Date) - needs[0].updated_ms > EXPIRATION_PERIOD_MS ) {
                  needsMap.delete(needs[0].uuid);
                  needs.shift();
              }
              //SAMM MQTT adapter can yet not be told to set the retain option.
              //if (needs.length != needs_length_before) {
                publish_payload("needs", needs);
              //}
              //haves_length_before = haves.length;              
              while (haves.length > 0 && (new Date) - haves[0].updated_ms > EXPIRATION_PERIOD_MS ) {
                  havesMap.delete(haves[0].uuid);
                  haves.shift();
              }
              //if (haves.length != haves_length_before) {
                publish_payload("haves", haves);      
              //}        
            }
          }
    } catch (e) {
        console.log(JSON.stringify(Object.assign({}, error_template, {message: e.stack.toString()})));
    }
});

function publish_payload (topic, payload) {
  message_template.created_at = new Date().toISOString();              
  result = Object.assign({}, message_template, {payload: payload});
  result.topic = obj.NAMESPACE_PUBLISHER + '/' + topic;
  console.log(JSON.stringify(result));
  
}